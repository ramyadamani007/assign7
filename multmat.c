/******************
	Include
******************/
#include<stdio.h>
#include<stdlib.h>

/* main */

int main()
{
	int x,y,z,sum,a[15][15],b[15][15],mult[15][15];
/********* accepting the elements for 3X3 matrix 1 *********/

	printf("Enter those values in a[3][3] for matrix 1.\n");
	for (x=0; x<3; x++)
		{
		for (y=0;y<3;y++)
			{
			scanf("%d",&a[x][y]);
			}
		}
	
/********* printing the elements matrix 1 *********/
	printf("Your entered values for matrix 1 are: \n");
	for (x=0; x<3; x++)
		{
		for (y=0; y<3; y++)
			{
			printf("%d\t",a[x][y]);
			}
		printf("\n");
		}

/********* accepting the elements matrix 2 *********/
	
	printf("Enter those values in b[3][3] for matrix 2.\n");
	for (x=0; x<3; x++)
		{
		for (y=0; y<3; y++)
			{
			scanf("%d",&b[x][y]);
			}
		}
	
/********* printing the elements of matrix 2 *********/
	printf("Your entered values for matrix 2 are: \n");
	for (x=0; x<3; x++)
		{
		for (y=0; y<3; y++)
			{
			printf("%d\t",b[x][y]);
			}
		printf("\n");
		}
	
/********* multiplication of both matrix *********/
	
	printf("multiplication of two matrix is: \n");
	for (x=0; x<3; x++)
		{
		for (y=0; y<3; y++)
			{
			sum = 0;
			for (z=0; z<3; z++)
				{
				sum = sum + a[x][z] * b[z][y];
				}
			mult[x][y] = sum;
			}
		}
	
	for (x=0; x<3; x++)
		{
		for (y=0; y<3; y++)
			{
			printf("%d\t",mult[x][y]);
			printf("\t");
			}
		printf("\n");
		}
}
