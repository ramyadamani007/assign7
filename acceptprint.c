/******************
	Include
******************/
#include<stdio.h>
#include<stdlib.h>

/** function declaration **/
void acceptprint (void);

/* main */

int main()
{
	acceptprint ();	/* function call */
	return 0;
}

/** function initialization **/
void acceptprint ()
	{
	int row,col,x,y,a[20][20];
	printf("Enter the number of values you want to enter in row and col.\n");
	scanf("%d %d",&row,&col);
	printf("Enter those values in a[%d][%d].\n",row,col);
	for (x=0; x<row; x++)
		{
		for (y=0;y<col;y++)
			{
			scanf("%d",&a[x][y]);
			}
		}
	
/********* printing the elements *********/
	printf("Your entered values are: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			printf("%d\t",a[x][y]);
			}
		printf("\n");
		}
	}
	
	
