/******************
	Include
******************/
#include<stdio.h>
#include<stdlib.h>

void rowadd (void);	/* Function Declaration */

/* main */
int main()
{
	rowadd();		/** Function Call **/
	printf("\n");
	return 0;
}



/** Function Initialization **/
void rowadd (void)	
{
	int row,col,x,y,z,sum = 0,a[20][20];

/***** Accepting the elements for matrix a *****/

	printf("Enter the number of row and col for matrix a.\n");
	scanf("%d %d",&row,&col);
	
	printf("Enter those values in a[%d][%d].\n",row,col);
	for (x=0; x<row; x++)
		{
		for (y=0;y<col;y++)
			{
			scanf("%d",&a[x][y]);
			}
		}
		
/********* printing the elements matrix a *********/
	printf("Your entered values for matrix a are: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			printf("%d\t",a[x][y]);
			}
		printf("\n");
		}
		
/********* printing the sum of elements of first row of matrix a *********/
	
	for (x=0; x<row; x++)
		{
		sum = 0;
		for (y=0; y<col; y++)
			{
			sum = sum + a [x][y];
			}
		 printf("The Sum of Elements of a Row in a Matrix =  %d \n", sum );
		}
}
