/*******************
	Include
*******************/

#include<stdio.h>
#include<stdlib.h>

/*****function declaration*****/
void todmat (void);
void fun (void);

/*****main()*******/

int main()
{
	todmat();	/*****function1 call*****/
 	fun();		/*****function2 call*****/
 	return 0;
}

/*****function1 initialization*****/

void todmat (void) 
{
/*******variable declaration*******/
	int x,y,row,col;
	printf("Enter the number of row and column.\n");
	scanf("%d %d",&row,&col);
	int *a[row];
	for (x=0; x<row; x++)
	a[x] = (int*) malloc (col*sizeof(int));
	printf("Enter the values of matrix: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			scanf("%d",&a[x][y]);
			}
		printf("\n");
		}
	
	printf("The matrix entered is: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			printf("%d\t",a[x][y]);
			}
		printf("\n");
		}
}

/*****function2 initialization*****/
void fun ()
{
int *a;
free (a);
}





