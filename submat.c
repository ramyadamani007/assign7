/******************
	Include
******************/
#include<stdio.h>
#include<stdlib.h>

/** function declaration **/
void submat (void);

/* main */

int main()
{
	submat ();	/* function call */
	return 0;
}

/** function initialization **/
void submat ()
	{
	int row,col,x,y,a[20][20],b[20][20],sub[20][20];
/********* accepting the elements matrix 1 *********/

	printf("Enter the number of values you want to enter in row and col for matrix 1 and matrix 2.\n");
	scanf("%d %d",&row,&col);
	printf("Enter those values in a[%d][%d] for matrix 1.\n",row,col);
	for (x=0; x<row; x++)
		{
		for (y=0;y<col;y++)
			{
			scanf("%d",&a[x][y]);
			}
		}
	
/********* printing the elements matrix 1 *********/
	printf("Your entered values for matrix 1 are: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			printf("%d\t",a[x][y]);
			}
		printf("\n");
		}

/********* accepting the elements matrix 2 *********/
	
	printf("Enter those values in b[%d][%d] for matrix 2.\n",row,col);
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			scanf("%d",&b[x][y]);
			}
		}
	
/********* printing the elements of matrix 2 *********/
	printf("Your entered values for matrix 2 are: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			printf("%d\t",b[x][y]);
			}
		printf("\n");
		}
	
/********* addition of both matrix *********/
	
	printf("subtraction of two matrices is: \n");
	for (x=0; x<row; x++)
		{
		for (y=0; y<col; y++)
			{
			sub[x][y] = a[x][y] - b[x][y];
			printf("%d\t", sub[x][y]);
			}
		printf("\n");
		}
	printf("\n");
	}
